#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Json {
    Number(f64)
}

impl std::str::FromStr for Json {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Json::Number(
            s.parse::<f64>().map_err(|_| ())?
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_integer() {
        let integer = "42";
        let result = integer.parse::<Json>().unwrap();
        assert_eq!(Json::Number(42.0), result);
    }

    #[test]
    fn parse_negative_integer() {
        let integer = "-1";
        let result = integer.parse::<Json>().unwrap();
        assert_eq!(Json::Number(-1.0), result);
    }

    #[test]
    fn parse_float() {
        let float = "3.1415";
        let result = float.parse::<Json>().unwrap();
        assert_eq!(Json::Number(3.1415f64), result);
    }

    #[test]
    fn parse_number_invalid_characters() {
        let invalid = "1a2";
        let result = invalid.parse::<Json>();
        assert!(result.is_err());
    }

    #[test]
    fn parse_number_two_decimals() {
        let invalid = "100.2.3";
        let result = invalid.parse::<Json>();
        assert!(result.is_err());
    }
}
